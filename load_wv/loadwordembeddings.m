vv='../temp/vocab.skip50w5-wiki.en';
ww='../temp/vects.skip50w5-wiki.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipwiki50.mat');

vv='../temp/vocab.skip50w5-blliplc.en';
ww='../temp/vects.skip50w5-blliplc.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipbll50.mat');

vv='../temp/vocab.skip25w5-blliplc.en';
ww='../temp/vects.skip25w5-blliplc.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipbll25.mat');

vv='../temp/vocab.skip100w5-wiki.en';
ww='../temp/vects.skip100w5-wiki.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipwiki100.mat');

vv='../temp/vocab.skip200w5-wiki.en';
ww='../temp/vects.skip200w5-wiki.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipwiki200.mat');

vv='../temp/vocab.skip500w5-wiki.en';
ww='../temp/vects.skip500w5-wiki.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipwiki500.mat');

vv='../temp/vocab.skip1000w5-wiki.en';
ww='../temp/vects.skip1000w5-wiki.en';
[words,We_orig]=saveTextAsMat(vv,ww);
save('../core_data/skipwiki1000.mat');