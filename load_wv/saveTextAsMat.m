function [v,w] = saveTextAsMat(vocabf,vectorf)
fid=fopen(vocabf);
x=textscan(fid,'%s');
x=x{1};
v=x';

w=dlmread(vectorf,' ');
w=w';
end