import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class make_constraint_matrix {

	public static void main(String[] args) {

		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		String fname = "../word_data_done/words.txt";
		HashMap<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
		HashSet<String> vocab = new HashSet<String>();

		HashMap<String,String> cache = new HashMap<String, String>();
		
		try {
			File file = new File(fname);
			FileReader fileReader = new FileReader(file);
			@SuppressWarnings("resource")
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			int count = 0;
			String line;
			System.out.println("Reading...");
			while ((line = bufferedReader.readLine()) != null) {
				String[] arr = line.split("\t");
				String w1 = arr[0].trim();
				String w2 = arr[1].trim();
				//System.out.println(w1+" "+w2);
				addToDict(map,w1,w2);
				addToDict(map,w2,w1);
				vocab.add(w1);
				vocab.add(w2);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		//Now add stems
		int counter =0;
		for(String s: map.keySet()) {
			counter++;
			//System.out.println(counter+" "+map.keySet().size());
			ArrayList<String> lis = map.get(s);
			lis.add(s);
			//check if stem in iis is same as any in vocab and add it
			ArrayList<String> newlis = new ArrayList<String>();
			for(String s1: lis) {
				String stem = getStem(s1,pipeline,cache);
				//String stem = getStem(s,pipeline,cache);
				newlis.add(s1);
				newlis.add(stem);
				for(String s2: vocab) {
					String stem2 = getStem(s2,pipeline,cache);
					if(stem.equals(stem2)) {
						newlis.add(s2);
					}
				}
			}
			//lis.add(stem);
			//map.put(s,lis);
			map.put(s, newlis);
		}
		
		for(String s: map.keySet()) {
			ArrayList<String> lis = map.get(s);
			HashSet<String> set = new HashSet<String>(lis);
			String l=s+"\t";
			for(String ss: set) {
				l=l+ss+"\t";
			}
			l=l.trim();
			System.out.println(l);
		}
	}
	
	public static String getStem(String ss, StanfordCoreNLP pipeline, HashMap<String, String> cache) {
		if(cache.containsKey(ss)) {
			return cache.get(ss);
		}
		Annotation document = new Annotation(ss);

		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);

		for(CoreMap sentence: sentences) {

			List<CoreLabel> lis = sentence.get(TokensAnnotation.class);
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {

				String word = token.get(TextAnnotation.class);
				String pos = token.get(PartOfSpeechAnnotation.class);
				String lem = token.get(LemmaAnnotation.class);
				cache.put(ss,lem);
				return lem;
			}
		}
		
		cache.put(ss, ss);
		return ss;
	}
	
	public static void addToDict(HashMap<String, ArrayList<String>> map, String w, String value) {
		if(map.containsKey(w)) {
			ArrayList<String> lis = map.get(w);
			lis.add(value);
			map.put(w, lis);
		}
		else {
			ArrayList<String> lis = new ArrayList<String>();
			lis.add(value);
			map.put(w,lis);
		}
	}

}