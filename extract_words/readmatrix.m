%load('word_matrix.m')
keys = [];
values = [];
[r c]=size(word_matrix);
for i=1:1:r
    k=word_matrix(i,1);
    v={};
    for j=1:1:c
        w=word_matrix(i,j);
        if(~isempty(w{1}))
            v{end+1}=w;
        end
    end
    keys=[keys k];
    values= [values;{v}];
end

newmap=containers.Map(keys,values);