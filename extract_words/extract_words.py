def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

f = open('../temp/vocab.skip25w5-wiki.en','r')
lines = f.readlines()

vocab = []
for i in lines:
    i=i.strip()
    vocab.append(i)

vocab = set(vocab)

f = open('../temp/ppdb-1.0-xl-lexical','r')
lines = f.readlines()

lis=[]

ct=0
for i in lines:
    i=i.split('|||')
    w1 = i[1].strip()
    w2 = i[2].strip()
    if(hasNumbers(w1) == False and hasNumbers(w2) == False and w1 in vocab and w2 in vocab):
        if(w1 < w2):
            lis.append(w1+"\t"+w2)
        else:
            lis.append(w2+"\t"+w1)
    else:
        ct = ct + 1

print "Removed: ", ct
nn=len(lis)
print "Total: ",(ct + nn)
print len(lines)
lis = list(set(lis))
lis=sorted(lis, reverse=True)
print "Redundant: ", (nn-len(lis))
print "Remaining: ", len(lis)
for i in lis:
    print i

#print len(lis)
