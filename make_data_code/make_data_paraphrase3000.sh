
#train=phrases_large/phrases-s.txt
train=../temp/ppdb3000.txt
#train=phrases_filtered/filtered_training_100k.txt


cd python
python prep_ppdb.py ../$train
#
cd ..
#
cd stanford-parser-2011-09-14/
sh lexparser.sh ../$train-1 > ../$train-1-parsed
sh lexparser.sh ../$train-2 > ../$train-2-parsed
#
cd ..

cd matlab
/Applications/MATLAB_R2014a.app/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r "make_mat('../../core_data/skipwiki25.mat','../../core_data/theta_init_25.mat',25,'../$train-1-parsed','../$train-2-parsed','../$train-scores','../$train.mat');quit"

stty sane
cd ..
#rm $fname*
