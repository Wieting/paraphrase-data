import random
import sys

inputfile = sys.argv[1]
outputfile = sys.argv[1]

f = open(inputfile,"r")
lines = f.readlines()
phrases1 = []
phrases2 = []
scores = []

for i in lines:
	i = i.split("|||")
	p1 = i[0]
	p2 = i[1]
	phrases1.append(p1)
	phrases2.append(p2)
	scores.append(i[2])

#for i in range(len(phrases1)):
#	print phrases1[i]+"|||"+phrases2[i]+"|||"+scores[i]

fout1 = outputfile+"-1"
fout1 = open(fout1,"w")
fout2 = outputfile + "-2"	
fout2 = open(fout2,"w")
fout3 = open(outputfile+"-all","w")
scoref = open(outputfile+"-scores","w")

for i in range(len(lines)):
	fout1.write(phrases1[i].strip()+"\n")
	fout2.write(phrases2[i].strip()+"\n")
	fout3.write(phrases1[i]+"|||"+phrases2[i]+"|||"+scores[i]+"\n")
	scoref.write(scores[i].strip()+"\n")

