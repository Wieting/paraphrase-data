function [] = make_mat(we,model,hiddenSize,input1,input2,scores,output)
restoredefaultpath;
addpath('../../../Paraphrase_Project_code/single_W_code/code/core/');
load(we);
load(model);
if(hiddenSize == 100)
    We_orig = We2;
end

W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

parseTreesp1 = getParseTrees(input1,words);
parseTreesp2 = getParseTrees(input2,words);

[Treesp1, ~] = getTrees2(parseTreesp1, W1, W2, bw1, We_orig, hiddenSize);
[Treesp2, ~] = getTrees2(parseTreesp2, W1, W2, bw1, We_orig, hiddenSize);

data = {};
for i=1:1:length(Treesp1)
    data{end+1} = {Treesp1{i}; Treesp2{i}; scores(i)};
end

for i=1:1:


end
